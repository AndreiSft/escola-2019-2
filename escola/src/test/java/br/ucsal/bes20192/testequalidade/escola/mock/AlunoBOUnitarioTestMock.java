package br.ucsal.bes20192.testequalidade.escola.mock;

import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoObjectMother;
import br.ucsal.bes20192.testequalidade.escola.business.AlunoBO;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

@RunWith(MockitoJUnitRunner.class)
public class AlunoBOUnitarioTestMock {
	
	private DateHelper dateUtil;
	private @Mock AlunoDAO alunoDAOMock;
	private AlunoBO alunoBO;
	
	@Before
	public void setup() {
		dateUtil = new DateHelper();
		alunoBO = new AlunoBO(alunoDAOMock, dateUtil);
	}
	
	@Test
	public void testarCalculoIdadeAluno() {
		Integer matricula = 567;
		Integer expected = 16;
		
		Mockito.when(alunoDAOMock.encontrarPorMatricula(matricula)).thenReturn(AlunoObjectMother.alunoNascidoEm2003());
		
		Integer actual = alunoBO.calcularIdade(matricula);
		
		Assert.assertEquals(expected, actual);
	}
	
}

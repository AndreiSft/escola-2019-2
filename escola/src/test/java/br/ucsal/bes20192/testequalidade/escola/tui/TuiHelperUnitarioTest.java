package br.ucsal.bes20192.testequalidade.escola.tui;

import java.io.*;

import org.junit.*;

public class TuiHelperUnitarioTest {

	private TuiHelper tuiHelper = new TuiHelper();
	
	/**
	 * Verificar a obten��o do nome completo. Caso de teste: primeiro nome
	 * "Claudio" e sobrenome "Neiva" resulta no nome "Claudio Neiva".
	 */
	@Test
	public void testarObterNomeCompleto() {
		ByteArrayInputStream in = new ByteArrayInputStream("Claudio\nNeiva".getBytes());
		System.setIn(in);
		
		String atual = tuiHelper.obterNomeCompleto();
		String esperado = "Claudio Neiva";
		
		Assert.assertEquals(esperado, atual);
	}

	/**
	 * Verificar a obten��o exibi��o de mensagem. Caso de teste: mensagem "Tem
	 * que estudar." resulta em "Bom dia! Tem que estudar.".
	 */
	@Test
	public void testarExibirMensagem() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
		
		tuiHelper.exibirMensagem("Tem que estudar.");
		String atual = out.toString();
		
		String esperado = "Bom dia! Tem que estudar.\n";	
		
		Assert.assertEquals(esperado, atual);
	}

}

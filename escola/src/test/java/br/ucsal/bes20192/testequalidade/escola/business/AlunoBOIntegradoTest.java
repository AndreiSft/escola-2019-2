package br.ucsal.bes20192.testequalidade.escola.business;

import org.junit.*;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoObjectMother;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class AlunoBOIntegradoTest {

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 ter�
	 * 16 anos.
	 */
	
	private AlunoDAO alunoDAO = new AlunoDAO();
	private DateHelper dateUtil = new DateHelper();
	private AlunoBO alunoBO = new AlunoBO(alunoDAO, dateUtil);

	
	@Test
	public void testarCalculoIdadeAluno1() {
		Aluno aluno = AlunoObjectMother.alunoNascidoEm2003();
				
		Integer atual = alunoBO.calcularIdade(aluno.getMatricula());
		Integer esperado = 16;
		
		Assert.assertEquals(esperado, atual);
	}

	/**
	 * Verificar se alunos ativos s�o atualizados.
	 */
	
	@Test
	public void testarAtualizacaoAlunosAtivos() {
		Aluno aluno = AlunoObjectMother.alunoNascidoEm2003();

		alunoBO.atualizar(aluno);
	}

}

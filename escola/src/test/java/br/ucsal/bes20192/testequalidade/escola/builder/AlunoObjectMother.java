package br.ucsal.bes20192.testequalidade.escola.builder;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;

public class AlunoObjectMother {
		
	public static Aluno alunoNascidoEm2003() {
		Aluno aluno = new Aluno();
		aluno.setMatricula(567);
		aluno.setNome("Andrei");
		aluno.setSituacao(SituacaoAluno.ATIVO);
		aluno.setAnoNascimento(2003);
		
		return aluno;
	}
	
}

package br.ucsal.bes20192.testequalidade.escola.builder;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;

public class AlunoBuilder {
	
	private static final Integer DEFAULT_MATRICULA = 1;
	private static final String DEFAULT_NOME = "Aluno Builder";
	private static final SituacaoAluno DEFAULT_SITUACAO_ALUNO = null;
	private static final Integer DEFAULT_ANO_NASCIMENTO = null;
	
	private Integer matricula = DEFAULT_MATRICULA;
	private String nome = DEFAULT_NOME;
	private SituacaoAluno situacaoAluno = DEFAULT_SITUACAO_ALUNO;
	private Integer anoNascimento = DEFAULT_ANO_NASCIMENTO;
	
	
	public static AlunoBuilder aluno() {
		return new AlunoBuilder();
	}
	
	public AlunoBuilder comMatricula(Integer matricula) {
		this.matricula = matricula;
		return this;
	}
	
	public AlunoBuilder comNome(String nome) {
		this.nome = nome;
		return this;
	}
	
	public AlunoBuilder comAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
		return this;
	}
	
	public AlunoBuilder comSituacaoAtiva() {
		this.situacaoAluno = SituacaoAluno.ATIVO;
		return this;
	}
	
	public AlunoBuilder comSituacaoCancelada() {
		this.situacaoAluno = SituacaoAluno.CANCELADO;
		return this;
	}
	
	public Aluno builder() {
		Aluno aluno = new Aluno();
		aluno.setMatricula(matricula);
		aluno.setNome(nome);
		aluno.setSituacao(situacaoAluno);
		aluno.setAnoNascimento(anoNascimento);
		
		return aluno;
	}
	
}

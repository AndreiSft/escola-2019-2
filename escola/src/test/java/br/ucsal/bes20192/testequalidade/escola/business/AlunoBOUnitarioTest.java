package br.ucsal.bes20192.testequalidade.escola.business;

import org.junit.*;

import br.ucsal.bes20192.testequalidade.escola.builder.*;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class AlunoBOUnitarioTest {
	
	private DateHelper dateUtil = new DateHelper();
	
	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 ter� 16
	 * anos.
	 */
	
	@Test
	public void testarCalculoIdadeAluno1() {
		Aluno aluno01 = AlunoObjectMother.alunoNascidoEm2003();

		Integer atual = calcularIdade(aluno01); 
		Integer esperado = 16;
		
		Assert.assertEquals(esperado, atual);
		
	}
	
	@Test
	public void testarCalculoIdadeAluno2() {
		Aluno aluno02 = AlunoBuilder.aluno().comMatricula(20).comNome("Aluno").comAnoNascimento(2003).comSituacaoAtiva().builder();

		Integer atual = calcularIdade(aluno02); 
		Integer esperado = 16;
		
		Assert.assertEquals(esperado, atual);
	}

	/**
	 * Verificar se alunos ativos s�o atualizados.
	 */
	@Test
	public void testarAtualizacaoAlunosAtivos() {
	}
	
	private Integer calcularIdade(Aluno aluno) {
		return dateUtil.obterAnoAtual() - aluno.getAnoNascimento();
	}

}
